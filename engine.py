from random import shuffle

from settings import BOARD_SIZE

class Engine:

    def get_new_board(self):
        """Create a brand-new, blank board data structure."""
        board = []
        for i in range(BOARD_SIZE):
            board.append([" " for _ in range(1, BOARD_SIZE+1)])
        return board

    def is_on_board(self, x, y):
        """Return True if the coordinates are located on the board."""
        return (
            x >= 0
            and x <= BOARD_SIZE - 1
            and y >= 0
            and y <= BOARD_SIZE - 1
        )

    def is_valid_move(self, board, tile, xstart, ystart):
        """Return False if the player's move on space xstart, ystart is invalid.
        If it is a valid move, return a list of spaces that would become the player's
         if they made a move here."""
        if board[xstart][ystart] != " ":
            return False

        if tile == "X":
            other_tile = "O"
        else:
            other_tile = "X"

        tiles_to_flip = []
        for xdirection, ydirection in [
            [0, 1],
            [1, 1],
            [1, 0],
            [1, -1],
            [0, -1],
            [-1, -1],
            [-1, 0],
            [-1, 1],
        ]:
            x, y = xstart, ystart
            x += xdirection
            y += ydirection
            while self.is_on_board(x, y) and board[x][y] == other_tile:
                x += xdirection
                y += ydirection
                if self.is_on_board(x, y) and board[x][y] == tile:
                    while True:
                        x -= xdirection
                        y -= ydirection
                        if x == xstart and y == ystart:
                            break
                        tiles_to_flip.append([x, y])

        if len(tiles_to_flip) == 0:
            return False
        return tiles_to_flip

    def get_board_with_valid_moves(self, board, tile):
        """Return a new board with periods marking the valid moves the player can make."""
        board_copy = self.get_board_copy(board)

        for x, y in self.get_valid_moves(board_copy, tile):
            board_copy[x][y] = "."
        return board_copy

    def get_valid_moves(self, board, tile):
        """Return a list of [x,y] lists of valid moves for the given player on the given board."""
        valid_moves = []
        for x in range(BOARD_SIZE):
            for y in range(BOARD_SIZE):
                if self.is_valid_move(board, tile, x, y):
                    valid_moves.append([x, y])
        return valid_moves

    def get_score_of_board(self, board):
        """Determine the score by counting the tiles. Return a dictionary with keys 'X' and 'O'."""
        xscore = 0
        oscore = 0
        for x in range(BOARD_SIZE):
            for y in range(BOARD_SIZE):
                if board[x][y] == "X":
                    xscore += 1
                if board[x][y] == "O":
                    oscore += 1
        return {"X": xscore, "O": oscore}

    def get_player_tile(self, white_choice, black_choice):
        """Return a list with the player's tile as the first item
        and the computer's tile as the second."""
        if black_choice:
            return ["X", "O"]
        elif white_choice:
            return ["O", "X"]

    def make_move(self, board, tile, xstart, ystart):
        """Place the tile on the board at xstart, ystart and flip any of the opponent's pieces.
        Return False if this is an invalid move; True if it is valid."""
        tiles_to_flip = self.is_valid_move(board, tile, xstart, ystart)
        if tiles_to_flip is False:
            return False
        board[xstart][ystart] = tile
        for x, y in tiles_to_flip:
            board[x][y] = tile
        return True

    def get_board_copy(self, board):
        """Make a duplicate of the board list and return it."""
        board_copy = self.get_new_board()

        for x in range(BOARD_SIZE):
            for y in range(BOARD_SIZE):
                board_copy[x][y] = board[x][y]

        return board_copy

    def is_on_corner(self, x, y):
        """Return True if the position is in one of the four corners."""
        return (x == 0 or x == BOARD_SIZE - 1) and (
            y == 0 or y == BOARD_SIZE - 1
        )

    def get_player_move(self, board, player_tile, player_move):
        while True:
            move = player_move
            if (
                move != []
                and self.is_valid_move(board, player_tile, move[0], move[1])
                is not False
            ):
                break
        return move

    def get_computer_move(self, board, computer_tile):
        """Given a board and the computer's tile,
        determine where to move and return that move as an [x, y] list."""
        possible_moves = self.get_valid_moves(board, computer_tile)
        shuffle(possible_moves)

        for x, y in possible_moves:
            if self.is_on_corner(x, y):
                return [x, y]

        best_score = -1
        for x, y in possible_moves:
            board_copy = self.get_board_copy(board)
            self.make_move(board_copy, computer_tile, x, y)
            score = self.get_score_of_board(board_copy)[computer_tile]
            if score > best_score:
                best_move = [x, y]
                best_score = score
        return best_move

    def show_score(self, board, player_tile, computer_tile):
        scores = self.get_score_of_board(board)
        return f"You  [size=46]{scores[player_tile]}[/size]  vs  CPU  [size=46]{scores[computer_tile]}[/size]"

    def show_final_score(self, board, player_tile, computer_tile):
        scores = self.get_score_of_board(board)
        if scores[player_tile] > scores[computer_tile]:
            message = (
                f"Congratulations!\nYou beat the Computer by "
                f"[b]{scores[player_tile] - scores[computer_tile]}[/b] points!"
            )
        elif scores[player_tile] < scores[computer_tile]:
            message = (
                f"You lost.\nThe Computer beat you by "
                f"[b]{scores[computer_tile] - scores[player_tile]}[/b] points."
            )
        else:
            message = "The game was a tie!"
        return message
        
    def player_win(self, board, player_tile, computer_tile):
        scores = self.get_score_of_board(board)
        if scores[player_tile] > scores[computer_tile]:
            return True
        else:
            return False
