rules_list = [
    "[b]Rules[/b]\n",
    "\nReversi is a strategy board game for two players, played on an 8×8 uncheckered board. ",
    "There are sixty-four identical game pieces called disks, which are light on ",
    "one side and dark on the other. Players take turns placing disks on the ",
    "board with their assigned color facing up. During a play, any disks of the ",
    "opponent's color that are in a straight line and bounded by the disk just placed ",
    "and another disk of the current player's color are turned over to the current ",
    "player's color. The objective of the game is to have the majority of disks ",
    "turned to display one's color when the last playable empty square is filled.\n",
    "\n[b]How to play[/b]\n",
    "\nOn Options Screen you can choose the side to play as (white or black) and ",
    "on/off show hints mode.\n",
    "\nOnce a game has started, use the touch-screen on the phone to select a ",
    "legal square in which to place a piece. For do CPU's move touch anywhere on ",
    "the game board.\n",
    "\nBelow the board shown whose move is it now and game score.\n\n\n",
]

RULES = "".join(rules_list)

start_message_list = [
'Year 4022. On space station "Ukraine-11" AI has got out of control. ',
"Now it's trying to turn off life support systems. Hack all levels of AI's defense and reload it."
]

START_MESSAGE = "".join(start_message_list)

victory_message_list = [
"Victory! You're hacked all levels of AI's defense and reloaded it. ",
"The lives of the station's residents are no longer in danger."
]

VICTORY_MESSAGE = "".join(victory_message_list)
