import time
import  json
from math import ceil

from kivymd.app import MDApp
from kivymd.uix.screen import MDScreen
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.list import OneLineIconListItem
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.popup import Popup
from kivy.core.text import LabelBase
from kivy.properties import (
    ObjectProperty,
    StringProperty,
    NumericProperty,
    ListProperty,
    BooleanProperty,
)
from kivy.uix.widget import Widget
from kivy.graphics import Line, Color, Rectangle, Ellipse
from kivy.utils import get_color_from_hex
from kivy.metrics import dp

from engine import Engine
from settings import BOARD_SIZE
from texts import RULES, START_MESSAGE, VICTORY_MESSAGE


class IconListItem(OneLineIconListItem):
    icon = StringProperty()
    
    
class MenuScreen(Screen):
    options_popup = ObjectProperty(None)

    def show_popup(self):
        self.options_popup = OptionsPopup()
        self.options_popup.open()


class OptionsPopup(Popup):
    show_hints_widget = ObjectProperty(None)
    
    def how_to_play_btn(self):
        self.dismiss()
        ReversiApp.screen_manager.current = "rules_screen"

    def on_dismiss(self):
        Table.show_hints = self.show_hints_widget.active
        Table.white_tiles = self.ids.white_tiles_id.active
        Table.black_tiles = self.ids.black_tiles_id.active


class RulesScreen(MDScreen):
    how_to_play = RULES
    
    
class GameScreen(MDScreen):
    start_popup = ObjectProperty(None)
    table = ObjectProperty(None)
    
    def show_start_popup(self):
        self.start_popup = StartPopup()
        self.start_popup.open()
        
    def on_enter(self):
        if self.table.start_game:
            self.table.start_game = False
            self.show_start_popup()
        self.table.start()
        
        menu_items = [
            {
                "viewclass": "IconListItem",
                "icon": "home-circle-outline",
                "text": "Main Menu",
                "height": dp(56),
                "on_release": lambda x="Main Menu": self.menu_callback(x),
             },
             {
                 "viewclass": "IconListItem",
                 "icon": "content-save",
                "text": "Save & Quit",
                "height": dp(56),
                "on_release": lambda x="Save & Quit": self.menu_callback(x),
             },
             {
                 "viewclass": "IconListItem",
                 "icon": "upload-outline",
                "text": "Load",
                "height": dp(56),
                "on_release": lambda x="Load": self.menu_callback(x),
             },
        ]
        self.menu = MDDropdownMenu(
            items=menu_items,
            width_mult=4,
        )
        
    def callback(self, button):
        self.menu.caller = button
        self.menu.open()

    def menu_callback(self, text_item):
        self.menu.dismiss()
        if text_item == "Main Menu":
            ReversiApp.screen_manager.current = "menu_screen"
        if text_item == "Save & Quit":
            self.table.save()
            ReversiApp.screen_manager.current = "menu_screen"
        if text_item == "Load":
            self.table.load()        
        

class StartPopup(Popup):
    start_popup_text = START_MESSAGE

        
class VictoryPopup(Popup):
    victory_popup_text = VICTORY_MESSAGE
    
    def press_btn(self):
        self.dismiss()
        ReversiApp.screen_manager.current = "menu_screen"
    
            
class EndgamePopup(Popup):
    final_score = StringProperty()

    def yes_btn(self):
        self.dismiss()
        ReversiApp.screen_manager.current = "menu_screen"
        ReversiApp.screen_manager.current = "game_screen"
        

    def no_btn(self):
        self.dismiss()
        ReversiApp.screen_manager.current = "menu_screen"

    
class Table(Widget):
    board = ObjectProperty(None)
    endgame_popup = ObjectProperty(None)
    victory_popup = ObjectProperty(None)
    show_hints = BooleanProperty(True)
    white_tiles = BooleanProperty(True)
    black_tiles = BooleanProperty(False)
    logic_board = ListProperty()
    game_info = StringProperty()
    score = StringProperty()
    player_move = ListProperty()
    player_tile = StringProperty()
    computer_tile = StringProperty()
    turn = StringProperty()
    win_counter = NumericProperty(0)
    start_game = BooleanProperty(True)

    engine = Engine()

    def show_endgame_popup(self):
        self.endgame_popup = EndgamePopup()
        self.endgame_popup.final_score = self.game_info
        self.endgame_popup.open()
        
    def show_victory_popup(self):
        self.victory_popup = VictoryPopup()
        self.victory_popup.open()
        
    def show_table(self):
        if self.show_hints:
            valid_moves_board = self.engine.get_board_with_valid_moves(
                self.logic_board, self.player_tile
            )
            self.board.update(valid_moves_board)
        else:
            self.board.update(self.logic_board)
            
        self.game_info = f"{self.turn}'s turn"
        self.score = self.engine.show_score(
            self.logic_board, self.player_tile, self.computer_tile
        )

    def start(self):
        self.player_tile, self.computer_tile = self.engine.get_player_tile(
            self.white_tiles, self.black_tiles
        )
        self.logic_board = self.engine.get_new_board()
        self.logic_board[3][3] = "X"
        self.logic_board[3][4] = "O"
        self.logic_board[4][3] = "O"
        self.logic_board[4][4] = "X"
        self.turn = "Player"
        self.show_table()

    def on_touch_down(self, touch):
        if self.board.collide_point(*touch.pos):
            cell_size = self.board.cell_size()
            x = ceil((touch.x - self.board.spacing) / cell_size[0]) - 1
            y = abs(
                ceil(
                    (
                    touch.y - self.board.spacing - self.board.center_y + self.board.height / 2
                    ) / cell_size[1]
                ) - 8
            )
            self.player_move = [x, y]
            self.play_game(
                self.logic_board, self.player_tile, self.computer_tile, self.player_move
            )

    def play_game(self, board, player_tile, computer_tile, player_move):
        player_valid_moves = self.engine.get_valid_moves(board, player_tile)
        computer_valid_moves = self.engine.get_valid_moves(board, computer_tile)

        if self.turn == "Player":
            if player_valid_moves:
                if self.engine.is_valid_move(
                    board, player_tile, player_move[0], player_move[1]
                ):
                    self.engine.make_move(
                        board, player_tile, player_move[0], player_move[1]
                    )
                    time.sleep(1)
                    self.turn = "Computer"
                    self.show_table()
            else:
                self.turn = "Computer"
                self.game_info = f"{self.turn}'s turn"

        elif self.turn == "Computer":
            if computer_valid_moves:
                move = self.engine.get_computer_move(board, computer_tile)
                self.engine.make_move(board, computer_tile, move[0], move[1])
                time.sleep(1)
                self.turn = "Player"
                self.show_table()
            else:
                self.turn = "Player"
                self.game_info = f"{self.turn}'s turn"

        player_valid_moves = self.engine.get_valid_moves(board, player_tile)
        computer_valid_moves = self.engine.get_valid_moves(board, computer_tile)

        if not player_valid_moves and not computer_valid_moves:
            self.game_info = self.engine.show_final_score(
                board, player_tile, computer_tile
            )
            if self.engine.player_win(board, player_tile, computer_tile):
                self.win_counter += 1
            if self.win_counter == 10:
                self.show_victory_popup()
                self.win_counter = 0
            else:
                self.show_endgame_popup()
            
    def save(self):
        to_json = {
            "board": self.logic_board,
            "player_tile": self.player_tile,
            "ai_tile": self.computer_tile,
            "turn": self.turn,
            "show_hints": self.show_hints,
            "win_counter": self.win_counter
        }
        with open("saves.json", "w") as f:
            json.dump(to_json, f)
            
    def load(self):
        try:
            with open("saves.json", "r") as f:
                from_json = json.load(f)
            self.logic_board = from_json["board"]
            self.player_tile = from_json["player_tile"]
            self.computer_tile = from_json["ai_tile"]
            self.turn = from_json["turn"]
            self.show_hints = from_json["show_hints"]
            self.win_counter = from_json["win_counter"]
            self.show_table()
        except FileNotFoundError:
            pass
        

class Board(Widget):
    spacing = NumericProperty(20)

    def all_cells(self):
        for x in range(BOARD_SIZE):
            for y in range(BOARD_SIZE):
                yield (x, y)

    def cell_size(self):
        return [
            (self.width - 2 * self.spacing) / BOARD_SIZE,
        ] * 2

    def cell_pos(self, board_x, board_y):
        cell_size = self.cell_size()
        return [
            self.x + board_x * cell_size[0] + self.spacing,
            (self.y + self.spacing + (
            BOARD_SIZE - 1
            ) * cell_size[1]) - board_y * cell_size[1],
        ]

    def draw_cell(self, board_x, board_y):
        cell_size = self.cell_size()
        cell_pos = self.cell_pos(board_x, board_y)
        cell_border_size = cell_size
        cell_border_pos = cell_pos
        with self.canvas:
            Color(*get_color_from_hex("518739"))
            Rectangle(pos=cell_pos, size=cell_size)
            Color(*get_color_from_hex("2d3033"))
            Line(
                width=2,
                rectangle=(
                    cell_border_pos[0],
                    cell_border_pos[1],
                    cell_border_size[0],
                    cell_border_size[1],
                ),
            )

    def draw_black_tile(self, board_x, board_y):
        cell_size = self.cell_size()
        cell_pos = self.cell_pos(board_x, board_y)
        cell_border_size = cell_size
        cell_border_pos = cell_pos
        tile_size = [i - 8 for i in cell_size]
        tile_pos = [i + 4 for i in cell_pos]
        with self.canvas:
            Color(*get_color_from_hex("518739"))
            Rectangle(pos=cell_pos, size=cell_size)
            Color(*get_color_from_hex("2d3033"))
            Line(
                width=2,
                rectangle=(
                    cell_border_pos[0],
                    cell_border_pos[1],
                    cell_border_size[0],
                    cell_border_size[1],
                ),
            )
            Color(*get_color_from_hex("000000"))
            Ellipse(pos=tile_pos, size=tile_size)

    def draw_white_tile(self, board_x, board_y):
        cell_size = self.cell_size()
        cell_pos = self.cell_pos(board_x, board_y)
        cell_border_size = cell_size
        cell_border_pos = cell_pos
        tile_size = [i - 8 for i in cell_size]
        tile_pos = [i + 4 for i in cell_pos]
        tile_border_size = tile_size
        tile_border_pos = tile_pos
        with self.canvas:
            Color(*get_color_from_hex("518739"))
            Rectangle(pos=cell_pos, size=cell_size)
            Color(*get_color_from_hex("2d3033"))
            Line(
                width=2,
                rectangle=(
                    cell_border_pos[0],
                    cell_border_pos[1],
                    cell_border_size[0],
                    cell_border_size[1],
                ),
            )
            Color(*get_color_from_hex("ffffff"))
            Ellipse(pos=tile_pos, size=tile_size)
            Color(*get_color_from_hex("000000"))
            Line(
                width=1,
                ellipse=(
                    tile_border_pos[0],
                    tile_border_pos[1],
                    tile_border_size[0],
                    tile_border_size[1],
                ),
            )

    def draw_hints_tile(self, board_x, board_y):
        cell_size = self.cell_size()
        cell_pos = self.cell_pos(board_x, board_y)
        cell_border_size = cell_size
        cell_border_pos = cell_pos
        circle_center_x = cell_pos[0] + cell_size[0] / 2
        circle_center_y = cell_pos[1] + cell_size[1] / 2
        with self.canvas:
            Color(*get_color_from_hex("518739"))
            Rectangle(pos=cell_pos, size=cell_size)
            Color(*get_color_from_hex("2d3033"))
            Line(
                width=2,
                rectangle=(
                    cell_border_pos[0],
                    cell_border_pos[1],
                    cell_border_size[0],
                    cell_border_size[1],
                ),
            )
            Color(*get_color_from_hex("ffffff"))
            Line(
                width=2,
                circle=(circle_center_x, circle_center_y, cell_size[0] / 6, 0, 360),
            )

    def draw(self, board):
        with self.canvas:
            Color(*get_color_from_hex("d0ae75"))
            Rectangle(pos=self.pos, size=self.size)
        for x, y in self.all_cells():
            if board[x][y] == "O":
                self.draw_white_tile(x, y)
            elif board[x][y] == "X":
                self.draw_black_tile(x, y)
            elif board[x][y] == ".":
                self.draw_hints_tile(x, y)
            else:
                self.draw_cell(x, y)

    def update(self, board):
        self.canvas.clear()
        self.draw(board)


class ReversiApp(MDApp):
    screen_manager = ObjectProperty(None)

    def build(self):
        self.theme_cls.theme_style = "Dark"
        ReversiApp.screen_manager = ScreenManager(transition=FadeTransition())
        menu = MenuScreen(name="menu_screen")
        game = GameScreen(name="game_screen")
        rules = RulesScreen(name="rules_screen")
        self.screen_manager.add_widget(menu)
        self.screen_manager.add_widget(game)
        self.screen_manager.add_widget(rules)

        return self.screen_manager


if __name__ == "__main__":
    LabelBase.register(
        name="Rubik",
        fn_regular="fonts/Rubik-Regular.ttf",
        fn_bold="fonts/Rubik-Bold.ttf",
        fn_italic="fonts/Komika_display.ttf",
    )
    ReversiApp().run()
