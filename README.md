# Space Reversi :star2:
It's a reversi game, that shows my progress in studying kivy fremwork. Based on the [Chapter 15 - The Reversegam Game](https://inventwithpython.com/invent4thed/chapter15.html) of the book  "Invent with Python". Watch what it looks like [on YouTube.](https://youtu.be/taWpOAVwgg8)

# Prerequisites
Project used Kivy and KivyMD.
Project dependiencies are listed in `requirements.txt`.
